# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
AuthorizationApp::Application.config.secret_key_base = '84cc5615a65c567b7c7b8d5ce8bcb978a6a06a6b19612616ded2547c5d0925b039ba885248f2eceb89a660fdfe598180c96204bb170af6a9211537dd1c4ac20b'
