module SessionsHelper

	def check_domain(user)
			current_domain = request.env['HTTP_HOST'].split('.').first
			#предполагается, что в модели пользователя существует атрибут domain
			user.domains.each do |d|
				return true if d.domain == current_domain
			end
			return false
	end
end