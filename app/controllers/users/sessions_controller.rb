class Users::SessionsController < Devise::SessionsController

	include SessionsHelper

	def new
		super
	end

	def create
		@use = User.find_by_email(params[:user][:email])
		unless check_domain(@use)
			redirect_to root_path, :notice => "You haven't access to this domain!" 
			return
		end
		self.resource = warden.authenticate!(auth_options)
    	set_flash_message(:notice, :signed_in) if is_flashing_format?
    	sign_in(resource_name, resource)
    	yield resource if block_given?
    	respond_with resource, location: after_sign_in_path_for(resource)
	end

	def destroy
		super
	end
end