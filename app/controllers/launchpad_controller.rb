class LaunchpadController < ApplicationController
	def index
		# если пользователь забыл, какие домены для него доступны, то возможны два варианта развития событий:
		if current_user.domains.count < 3
			# у пользователя лишь два дозволенных домена: launchpad и %domain_name%, причем %domain_name%
			# всегда является вторым
			current_url = request.url.split(".")
			current_url.collect! do |i|
				(i == "launchpad") ? i = current_user.domains.all.second.domain : i
			end
			new_url = current_url.join(".")
			redirect_to new_url
		else
			# пользователю принадлежит множество доступных доменов и в таком случае пользователя перекидывает
			# на страницу со списком доступных ему доменов
			redirect_to :action => "show", :id => 1
		end
	end

	def show
	end
end
